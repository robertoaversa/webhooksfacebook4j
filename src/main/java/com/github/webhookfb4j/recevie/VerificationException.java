package com.github.webhookfb4j.recevie;


/**
 * Thrown to indicate that a verification failed.
 *
 * <p>
 * For example verification of the payload signature.
 * </p>
 *
 * @author Roberto Aversa
 * @since 0.0.1
 */
public class VerificationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

}
