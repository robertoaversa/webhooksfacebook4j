package com.github.webhookfb4j.recevie;


/**
 * A {@code WebhooksTopicsReceive }is responsible for handling all topics from the Facebook Webhooks.
 *
 * <p>
 * It handles all inbound messages and events, and validates the integrity and origin of the payload.<br>
 * Furthermore it provides the functionality to verify the {@code Webhook}.
 * </p>
 *
 * <p>
 * For further information about topics on  {@code Webhooks} refer to:<br>
 * <a href="https://developers.facebook.com/docs/graph-api/webhooks/reference">
 * https://developers.facebook.com/docs/graph-api/webhooks/reference
 * </a>
 * </p>
 *
 * @author Roberto Aversa
 * @since 0.0.1
 * @see WebhooksTopicsReceiveBuilder
 */
public interface WebhooksTopicsReceive {

    /**
     * Supports the {@code Webhook} verification process by comparing the {@code verification token} and {@code mode}.
     *
     * <p>
     * The return value of this method has to be returned as response to the verification request.
     * </p>
     *
     * @param mode        the {@code hub.mode} query parameter
     * @param verifyToken the {@code hub.verify_token} query parameter
     * @param challenge   the {@code hub.challenge} query parameter
     * @return the {@code challenge} if the verification was successful
     * @throws MessengerVerificationException thrown if the verification was <b>not</b> successful
     */
    String verifyWebhook(String mode, String verifyToken, String challenge)
            throws VerificationException;

    /**
     * Processes the callback payload and call the right {@link FieldHandler}.
     *
     * <p>
     * When using this method you have to disable the {@code signature} verification
     * (see {@link WebhooksTopicsReceiveBuilder}), otherwise a {@link IllegalArgumentException} is thrown.
     * </p>
     *
     * <p>
     * <b>It is NOT recommended to disable the signature verification!</b>
     * </p>
     *
     * @param payload the request body {@code JSON payload}
     */
    void processCallbackPayload(String payload);

    /**
     * Processes the callback payload and call the right {@link FieldHandler}.
     * Also the integrity and origin of the payload is ensured by validating the given {@code signature}.
     *
     * @param payload the request body {@code JSON payload}
     * @param signature the {@code X-Hub-Signature} header value. SHA1 signature of the request payload.
     * @throws VerificationException thrown if the verification of the {@code signature} fails
     */
    void processCallbackPayload(String payload, String signature) throws VerificationException;
}