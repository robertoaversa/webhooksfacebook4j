package com.github.webhookfb4j.recevie.fieldupdatehandlers;

import com.github.webhookfb4j.recevie.filedupdate.FiledPageUpdate;

public interface  FieldPageUpdateHandler<U extends FiledPageUpdate> {
	
	public void handle(U fieldPageUpdate);
	
}
