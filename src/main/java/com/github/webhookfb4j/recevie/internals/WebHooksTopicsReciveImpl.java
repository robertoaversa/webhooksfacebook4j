package com.github.webhookfb4j.recevie.internals;

import java.util.List;

import com.github.webhookfb4j.receive.callers.FieldUpdateHandlerCaller;
import com.github.webhookfb4j.recevie.VerificationException;
import com.github.webhookfb4j.recevie.WebhooksTopicsReceive;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class WebHooksTopicsReciveImpl implements WebhooksTopicsReceive {
	private final JsonParser jsonParser;
	private final List<FieldUpdateHandlerCaller> eventHandlerCallers;
	

	public WebHooksTopicsReciveImpl(JsonParser jsonParser, List<FieldUpdateHandlerCaller> eventHandlerCallers) {
		super();
		this.jsonParser = jsonParser;
		this.eventHandlerCallers = eventHandlerCallers;
	}

	@Override
	public String verifyWebhook(String mode, String verifyToken, String challenge) throws VerificationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void processCallbackPayload(String payload) {
		final JsonObject payloadJsonObject = this.jsonParser.parse(payload).getAsJsonObject();
		
	}

	@Override
	public void processCallbackPayload(String payload, String signature) throws VerificationException {
		// TODO Auto-generated method stub

	}

}
